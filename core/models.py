from django.contrib.auth.models import User
from django.db import models


class TimeStampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TimeStampCreatorModel(TimeStampModel):
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    draft = models.BooleanField(default=False)

    class Meta:
        abstract = True