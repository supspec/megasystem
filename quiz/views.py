from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.decorators import api_view, action
from rest_framework.response import Response

from . import models
from . import serializers
from .models import Question, Choice, Answer, Customer
from .permissions import IsAdminUserOrReadOnly, IsAminUserOrReadOnlyWithoutList
from .serializers import QuestionSerializer, ChoiceSerializer, FullQuestionSerializer, FullChoiceSerializer, \
    AnswerSerializer


class QuizViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.QuizSerializer
    # queryset = models.Quiz.objects.all()

    permission_classes = [IsAdminUserOrReadOnly]

    def get_queryset(self):
        if self.request.user.is_staff:
            return models.Quiz.objects.all()
        else:
            return (item for item in models.Quiz.objects.filter(draft=False) if item.active)

    def perform_create(self, serializer):
        serializer.validated_data['creator'] = self.request.user
        super().perform_create(serializer)


class QuestionViewSet(viewsets.ModelViewSet):
    def get_serializer_class(self):
        if self.request.user.is_staff:
            return FullQuestionSerializer
        return QuestionSerializer

    def get_queryset(self):
        return Question.objects.filter(quiz=self.kwargs['quiz_pk'])


    permission_classes = [IsAdminUserOrReadOnly]

    def perform_create(self, serializer):
        serializer.validated_data['creator'] = self.request.user
        super().perform_create(serializer)

    @action(detail=True, methods=['post'])
    def answer(self, request, quiz_pk, pk):
        question = self.get_object()
        data = request.data
        customer_id = data.get('customer_id', None)
        if customer_id:
            try:
                customer = models.Customer.objects.get(pk=customer_id)
            except models.Customer.DoesNotExist:
                customer = models.Customer.objects.create(pk=customer_id)
            data['question'] = question.pk
            serializer = serializers.CreateAnswerSerializer(data=data)
            if serializer.is_valid():
                r = serializer.save()
                return Response({'status': AnswerSerializer(r).data})
            else:
                return Response(serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = serializers.CreateAnswerSerializer(data=data)
            if serializer.is_valid():
                pass
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChoiceViewSet(viewsets.ModelViewSet):

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return FullChoiceSerializer
        return ChoiceSerializer

    def get_queryset(self):
        return Choice.objects.filter(question=self.kwargs['question_pk'])

    permission_classes = [IsAdminUserOrReadOnly]

    def perform_create(self, serializer):
        question = Question.objects.get(pk=self.kwargs['question_pk'])
        serializer.validated_data['question'] = question
        super().perform_create(serializer)


#
class CustomerViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CustomerSerializer
    queryset = models.Customer.objects.all()

    permission_classes = [IsAminUserOrReadOnlyWithoutList]
    http_method_names = ['get']
    # def list(self, request, *args, **kwargs):
    #     return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

