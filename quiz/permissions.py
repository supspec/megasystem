from rest_framework.permissions import IsAdminUser, SAFE_METHODS, BasePermission


class IsAdminUserOrReadOnly(IsAdminUser):
    def has_permission(self, request, view):
        # print(view.name)
        if view.action == 'answer':
            return True
        is_admin = super().has_permission(request, view)
        return (request.method in SAFE_METHODS) or is_admin


class IsAminUserOrReadOnlyWithoutList(IsAdminUser):

    def has_permission(self, request, view):
        is_admin = super().has_permission(request, view)
        if not is_admin and view.action == 'list':
            return False
        return (request.method in SAFE_METHODS) or is_admin


# class ReadAndCreate(BasePermission):

