from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

class Command(BaseCommand):

    def handle(self, *args, **options):
        USER = get_user_model()
        if not USER.objects.filter(username='admin').exists():
            USER.objects.create_superuser('admin', 'admin@admin.ru', 'admin')
        else:
            print("User admin with password admin already exists!")
