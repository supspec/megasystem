from django.contrib import admin
from . import models


@admin.register(models.Quiz)
class QuizAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'start_date', 'end_date', 'draft')
    list_editable = ('draft',)
    readonly_fields = ('start_date',)


admin.site.register(models.Question)
admin.site.register(models.Choice)
admin.site.register(models.Answer)
admin.site.register(models.Customer)
