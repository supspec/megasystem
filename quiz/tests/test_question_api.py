import json
from datetime import timedelta

from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APITestCase
from rest_framework import status

from quiz.models import Quiz, Question
from quiz import serializers


class QuestionApiTestCases(APITestCase):

    def setUp(self) -> None:
        self.admin = User.objects.create(username='testadmin', is_staff=True)
        self.user = User.objects.create(username='user')
        self.quiz1 = Quiz.objects.create(name='Test', description='Python lambda function with no arguments',
                                         creator=self.admin)
        self.quiz2 = Quiz.objects.create(name='Last',
                                         description='Someone asked me if we can have a lambda function without any argument?',
                                         creator=self.admin)
        self.question1_1 = Question.objects.create(title='Test1', type='text', quiz=self.quiz1)
        self.question1_2 = Question.objects.create(title='Test2', type='radio', quiz=self.quiz1)
        self.question1_3 = Question.objects.create(title='Test3', type='checkbox', quiz=self.quiz1)

    def test_list_all_quiz_questions(self):
        url = reverse('quiz-questions-list', args=(self.quiz1.id,))
        resp = self.client.get(url)
        data = resp.data
        serialized_data = serializers.QuestionSerializer([self.question1_1, self.question1_2, self.question1_3], many=True).data
        self.assertEqual(status.HTTP_200_OK, resp.status_code)
        count = Question.objects.filter(quiz=self.quiz1).count()
        self.assertEqual(count, len(data))
        self.assertEqual(serialized_data, data)
