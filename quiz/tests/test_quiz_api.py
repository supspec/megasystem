import json
from datetime import timedelta

from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.test import APITestCase
from rest_framework import status

from quiz.models import Quiz, Question
from quiz import serializers


class QuizApiTestCases(APITestCase):

    def setUp(self) -> None:
        self.admin = User.objects.create(username='testadmin', is_staff=True)
        self.user = User.objects.create(username='user')
        self.quiz1 = Quiz.objects.create(name='Test', description='Python lambda function with no arguments',
                                         creator=self.admin)
        self.quiz2 = Quiz.objects.create(name='Last',
                                         description='Someone asked me if we can have a lambda function without any argument?',
                                         creator=self.admin)
        self.quiz3 = Quiz.objects.create(name='Quiz', description='')

    def test_list_all_quiz(self):
        url = reverse('quiz-list')

        resp = self.client.get(url)
        data = resp.data
        self.assertEqual(len(data), 0)

        self.client.force_login(self.admin)
        resp = self.client.get(url)
        data = resp.data
        serialized_data = serializers.QuizSerializer([self.quiz1, self.quiz2, self.quiz3], many=True).data
        self.assertEqual(status.HTTP_200_OK, resp.status_code)
        count = Quiz.objects.all().count()
        self.assertEqual(serialized_data, data)
        self.assertEqual(len(data), count)

    def test_create_quiz(self):
        url = reverse('quiz-list')
        data = {
            'name': 'Test Quiz',
            'description': 'Test Quiz Description'
        }
        count = Quiz.objects.all().count()
        json_data = json.dumps(data)
        resp = self.client.post(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_403_FORBIDDEN, resp.status_code)

        self.client.force_login(self.user)
        resp = self.client.post(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_403_FORBIDDEN, resp.status_code)
        self.client.logout()

        self.client.force_login(self.admin)
        resp = self.client.post(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, resp.status_code)
        new_count = Quiz.objects.all().count()
        self.assertEqual(count + 1, new_count)

        data = resp.data
        self.assertEqual(data.get('creator'), self.admin.id)

    def test_update_quiz(self):
        url = reverse('quiz-detail', args=(self.quiz1.id,))
        self.client.force_login(self.admin)
        data = {
            'id': self.quiz1.id,
            'name': self.quiz1.name,
            'description': 'New description',
            'start_date': self.quiz1.start_date,
            'end_date': self.quiz1.end_date,
            'creator': self.quiz1.creator.id,
            'draft': self.quiz1.draft,
            'active': self.quiz1.active
        }
        resp = self.client.put(url, data=json.dumps(data, cls=DjangoJSONEncoder), content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, resp.status_code)
        self.assertEqual('New description', resp.data.get('description'))
        # print(resp.status_code)
        old = resp.data
        data = {
            'id': self.quiz1.id,
            'name': self.quiz1.name,
            'description': 'New description',
            'start_date': self.quiz1.start_date + timedelta(minutes=10),
            'end_date': self.quiz1.end_date,
            'creator': self.quiz1.creator.id,
            'draft': self.quiz1.draft,
            'active': self.quiz1.active
        }
        resp = self.client.put(url, data=json.dumps(data, cls=DjangoJSONEncoder), content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, resp.status_code)
        self.assertEqual(old, resp.data)
        # print(data)
        # print(resp.data)
