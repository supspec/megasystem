from rest_framework import serializers
from . import models
from .models import Question, Choice


class FullChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('id', 'text', 'right')


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('id', 'text',)


class FullQuestionSerializer(serializers.ModelSerializer):
    choices = FullChoiceSerializer(many=True, required=False)

    class Meta:
        model = Question
        fields = ('id', 'title', 'question', 'right_answer_text', 'type', 'choices', 'quiz')

    def create(self, validated_data):
        if validated_data['type'] != 'text':
            validated_data['right_answer_text'] = ''
        return super().create(validated_data)


class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True, required=False)

    class Meta:
        model = Question
        fields = ('id', 'title', 'question', 'type', 'choices', 'quiz')


class QuizSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, required=False)

    class Meta:
        model = models.Quiz
        fields = ('id', 'name', 'description', 'start_date', 'end_date', 'creator', 'draft', 'active', 'questions')
        read_only_fields = ('start_date',)


class AnswerSerializer(serializers.ModelSerializer):
    choices = FullChoiceSerializer(many=True, required=False)
    question = QuestionSerializer()
    class Meta:
        model = models.Answer
        fields = ('id', 'answer_text', 'choices', 'question', 'customer_id', 'right')


class CreateAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answer
        fields = ('id', 'answer_text', 'choices', 'question', 'customer_id', 'right')

    def create(self, validated_data):
        answer = models.Answer(
            customer_id=validated_data['customer_id'],
            question=validated_data['question']
        )
        answer.save()
        if answer.question.type == 'radio':
            for choice in validated_data['choices']:
                if answer.question.pk != choice.question_id:
                    raise ValueError('Choice from another question')
                choice.answers = answer
                choice.save()
                if choice.right:
                    answer.right = True
            answer.save()
        elif answer.question.type == 'checkbox':
            choices = []
            for choice in validated_data['choices']:
                if answer.question.pk != choice.question_id:
                    raise ValueError('Choice from another question')
                choice.answers = answer
                choice.save()
                choices.append(choice.right)
            if all(choices):
                answer.right = True
            answer.save()
        elif answer.question.type == 'text':
            answer.answer_text = validated_data['answer_text']
            if answer.answer_text == answer.question.right_answer_text:
                answer.right = True
            answer.save()

        return answer


class CustomerSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, required=False)

    class Meta:
        model = models.Customer
        fields = ('id', 'answers',)
