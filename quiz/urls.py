from django.urls import include, re_path, path
from rest_framework import routers
from rest_framework_nested import routers as nested_routers
from rest_framework_swagger.views import get_swagger_view
from . import views


schema_view = get_swagger_view(title='Quiz API')

router = routers.SimpleRouter()
router.register('quiz', views.QuizViewSet, basename='quiz')
router.register('customer', views.CustomerViewSet, basename='customer')

question_router = nested_routers.NestedSimpleRouter(router, r'quiz', lookup='quiz')
question_router.register(r'question', views.QuestionViewSet, basename='quiz-questions')

choice_router = nested_routers.NestedSimpleRouter(question_router, r'question', lookup='question')
choice_router.register(r'choice', views.ChoiceViewSet, basename='question-choices')

answer_route = nested_routers.NestedSimpleRouter(question_router, r'question', lookup='question')
answer_route.register(r'answer', views.AnswerSerializer, basename='create_answer')

urlpatterns = [
    re_path(r'^$', schema_view),
    re_path(r'^', include(router.urls)),
    re_path(r'^', include(question_router.urls)),
    re_path(r'^', include(choice_router.urls)),

]
