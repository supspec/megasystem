from datetime import timedelta
from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _
from core.models import TimeStampModel, TimeStampCreatorModel


def time_zone_plus_1():
    return timezone.now() + timedelta(hours=1)


def time_zone_plus_2():
    return timezone.now() + timedelta(hours=2)


class Quiz(TimeStampCreatorModel):
    name = models.CharField(max_length=250, unique=True, db_index=True)
    description = models.TextField()
    start_date = models.DateTimeField(default=time_zone_plus_1)
    end_date = models.DateTimeField(default=time_zone_plus_2)

    @property
    def active(self):
        """ active if:
         1. the quiz not draft and has one or more active questions
         2. current date more than start_date and less than end_date
         """
        if not self.draft and self.questions.filter(draft=False).count():
            now = timezone.now()
            if self.start_date < now < self.end_date:
                return True
        return False

    def save(self, *args, **kwargs):
        # if self.start_date <= timezone.now():
        #     raise ValueError("start_date less than current date time.")
        if self.end_date <= self.start_date:
            raise ValueError("end_date can't be less or equal than start_date.")

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Question(TimeStampCreatorModel):
    TYPE = (
        ('text', _('Text Answer')),
        ('radio', _('One Choice')),
        ('checkbox', _('Multiply Choice')),
    )
    type = models.CharField(max_length=20, choices=TYPE, default='text')
    title = models.CharField(max_length=250)
    question = models.TextField(default='', blank=True)
    right_answer_text = models.CharField(max_length=200, blank=True)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='questions')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class Choice(TimeStampModel):
    text = models.CharField(max_length=200)
    right = models.BooleanField(default=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='choices')
    answers = models.ForeignKey('Answer', on_delete=models.DO_NOTHING, null=True, related_name='choices')
    def save(self, *args, **kwargs):
        if self.question.type == 'text':
            raise ValueError("We can't to add any choices to the text question.")
        if self.question.type == 'radio':
            if self.right and self.question.choices.filter(right=True):
                raise ValueError("We can't to add noe more right choices to this question.")
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.text}({self.right})"


class Answer(TimeStampModel):
    answer_text = models.CharField(max_length=200, blank=True)
    question = models.ForeignKey(Question, on_delete=models.SET_NULL, null=True, related_name='answers')
    # customer_id = models.IntegerField()
    customer_id = models.ForeignKey('Customer', on_delete=models.DO_NOTHING, related_name='answers')
    right = models.BooleanField(default=False)



class Customer(models.Model):
    pass
    # customer_id = models.IntegerField(unique=True, db_index=True)

