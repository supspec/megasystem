FROM python:3.8

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/megasystem

RUN pip install --upgrade pip
COPY . /usr/src/megasystem/
RUN pip install -r /usr/src/megasystem/requirements.txt

EXPOSE 8080
#CMD ['python', 'manage.py', 'migrate']
#CMD ['python', 'manage.py', 'runserver', '0.0.0.0:8080']
